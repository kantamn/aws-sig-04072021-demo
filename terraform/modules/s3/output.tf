output "info" {
  value = {
    bucket_id  = aws_s3_bucket.s3_bucket.id
    bucket_arn = aws_s3_bucket.s3_bucket.arn
    bucket_dn = aws_s3_bucket.s3_bucket.bucket_domain_name
    bucket_kms_key = aws_kms_key.s3_encryption.arn
    ssm_arn = aws_ssm_parameter.standard.0.arn
  }
}
