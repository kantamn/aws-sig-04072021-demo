resource "aws_vpc_endpoint" "af_esb" {
  count = var.vpc_endpoint ? 1 : 0
  vpc_id            = aws_vpc.standard.id
  service_name      = var.vpc_endpoint_service_name
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.vpc_endpoint_sg.id
  ]
}
