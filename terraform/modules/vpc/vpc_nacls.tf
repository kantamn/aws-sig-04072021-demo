# Add network ACL

resource "aws_network_acl" "public" {
  vpc_id     = aws_vpc.standard.id
  subnet_ids = aws_subnet.public.*.id
  tags = {
    Name      = "${var.name}-${var.env} - Public - VPC - Network ACLs"
    CreatedBy = local.creator
    Stage     = var.env
  }
}

resource "aws_network_acl_rule" "public_in" {
  count          = length(var.ucd_campus_cidrs)
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.vpc_config.rule_number_default + count.index
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = var.ucd_campus_cidrs[count.index]
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl_rule" "public_ports_in" {
  count = length(var.nacl_public_ports)
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.vpc_config.rule_number_public + count.index
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = var.nacl_public_ports[count.index]
  to_port        = var.nacl_public_ports[count.index]
}

resource "aws_network_acl_rule" "public_cidr_in" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.vpc_config.rule_number_default + 50
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = var.cidr
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl_rule" "public_all_in" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.vpc_config.rule_number_default + 55
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl_rule" "public_out" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.vpc_config.rule_number_default + 60
  egress         = true
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl" "private" {
  vpc_id     = aws_vpc.standard.id
  subnet_ids = aws_subnet.private.*.id
  tags = {
    Name      = "${var.name}-${var.env} - Private - VPC - Network ACLs"
    CreatedBy = local.creator
    Stage     = var.env
  }
}

resource "aws_network_acl_rule" "private_in" {
  network_acl_id = aws_network_acl.private.id
  rule_number    = var.vpc_config.rule_number_default + 65
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 65535
}

resource "aws_network_acl_rule" "private_out" {
  network_acl_id = aws_network_acl.private.id
  rule_number    = var.vpc_config.rule_number_default + 70
  egress         = true
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 65535
}
