resource "aws_vpc_dhcp_options" "domain" {
  count = var.domain_dhcp ? 1 : 0
  domain_name = var.domain_name
  domain_name_servers = var.domain_servers
  ntp_servers = var.domain_servers
  netbios_name_servers = var.domain_servers
  tags = {
		Name = "${var.name} - DHCP Options"
		CreatedBy = "Terraform"
		STAGE = var.env
	}
}

resource "aws_vpc_dhcp_options_association" "domain" {
  count = var.domain_dhcp ? 1 : 0
  vpc_id = aws_vpc.standard.id
  dhcp_options_id = aws_vpc_dhcp_options.domain[count.index].id
}
