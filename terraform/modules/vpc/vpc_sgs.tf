# Create inbound SSH SG for all campus CIDRs

resource "aws_security_group" "ssh_sg" {
  name        = "${var.env}-${var.name}-ssh-sg"
  description = "Allow inbound SSH for Bastion host"
  vpc_id      = aws_vpc.standard.id
  tags = {
    Stage     = var.env
    CreatedBy = local.creator
  }
}

resource "aws_security_group" "db_sg" {
    name = "${var.env}-${var.name}-db-in"
    description = "Allow Sophos ports inbound access from Sec VPC"
    vpc_id = aws_vpc.standard.id

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups = [aws_security_group.ec2_sg.id, aws_security_group.ssh_sg.id]
    }
    tags = {
      STAGE = var.env
      CreatedBy = "Terraform"
    }
}

resource "aws_security_group_rule" "ssh_in_campus_cidrs" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = var.ucd_campus_cidrs
  security_group_id = aws_security_group.ssh_sg.id
}

resource "aws_security_group_rule" "shh_in_custom_ip" {
  count             = length(var.custom_cidrs)
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [var.custom_cidrs[count.index]]
  security_group_id = aws_security_group.ssh_sg.id
}

resource "aws_security_group_rule" "ssh_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ssh_sg.id
}

# Create outbound SGs for internet (www) and VPC CIDR blocks

resource "aws_security_group" "vpc_out_sg" {
  name        = "${var.env}-${var.name}-vpc-out"
  description = "Allow outbound access to VPC subnets"
  vpc_id      = aws_vpc.standard.id
  tags = {
    Stage     = var.env
    CreatedBy = local.creator
  }
}

resource "aws_security_group_rule" "vpc_out_1" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = [var.cidr]
  security_group_id = aws_security_group.vpc_out_sg.id
}

resource "aws_security_group" "www_out_sg" {
  name        = "${var.env}-${var.name}-www-out"
  description = "Allow outbound access from VPC to internet"
  vpc_id      = aws_vpc.standard.id
  tags = {
    Stage     = var.env
    CreatedBy = local.creator
  }
}

resource "aws_security_group_rule" "www_out_1" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.www_out_sg.id
}

resource "aws_security_group" "ec2_sg" {
  name        = "${var.env}-${var.name}-ec2-sg"
  description = "Allow in/out bound traffic for EC2"
  vpc_id      = aws_vpc.standard.id
  tags = {
    Stage     = var.env
    CreatedBy = local.creator
  }
}

resource "aws_security_group_rule" "ec2_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_sg.id
}

resource "aws_security_group_rule" "ec2_in" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = [var.cidr]
  security_group_id = aws_security_group.ec2_sg.id
}

resource "aws_security_group" "alb_sg" {
  name        = "${var.env}-${var.name}-alb-sg"
  description = "SG for ALB"
  vpc_id      = aws_vpc.standard.id
  tags = {
    Stage     = var.env
    CreatedBy = local.creator
  }
}

resource "aws_security_group_rule" "alb_sg_in_80" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "alb_sg_in_443" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "alb_sg_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group" "vpc_endpoint_sg" {
  name        = "${var.env}-${var.name}-vpc-endpoint-sg"
  description = "SG for VPC Endpoint"
  vpc_id      = aws_vpc.standard.id
  tags = {
    Stage     = var.env
    CreatedBy = local.creator
  }
}

resource "aws_security_group_rule" "vpc_endpoint_in" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_endpoint_sg.id
}

resource "aws_security_group_rule" "vpc_endpoint_out" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_endpoint_sg.id
}
