# Handle public routes

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.standard.id
  tags = {
    Name     = "${var.name}-public-route-${var.env}"
    CreateBy = local.creator
    Stage    = var.env
  }
  depends_on = [aws_subnet.public]
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.standard.id
  depends_on             = [aws_internet_gateway.standard]
}

resource "aws_route_table_association" "public" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
  depends_on     = [aws_internet_gateway.standard]
}

# Handle private routes

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.standard.id
  tags = {
    Name     = "${var.name}-private-route-${var.env}"
    CreateBy = local.creator
    Stage    = var.env
  }
  depends_on = [aws_subnet.private]
}

resource "aws_route" "private" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.standard.id
  depends_on             = [aws_nat_gateway.standard]
}

resource "aws_route_table_association" "private" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private.id
  depends_on     = [aws_nat_gateway.standard]
}

