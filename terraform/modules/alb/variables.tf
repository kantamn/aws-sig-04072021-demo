variable "env" {}

variable "app" {}

variable "alb_log_bucket" {}

variable "vpc_id" {}

variable "vpc_name" {}

variable "cert" {}

variable "health_check_path" {}

variable "health_check_interval" {}

variable "health_check_timeout" {}

variable "store_in_ssm" {
  default = true
}

variable "public_subnet_ids" {}

variable "alb_sg" {
  default = ""
}
