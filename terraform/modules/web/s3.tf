resource "aws_s3_bucket" "www" {
  bucket = "${var.env}-${var.site_name}-www-${random_string.s3_tail.result}"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.s3_kms_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  # website {
  #   index_document = "index.html"
  # }

  tags = {
    Name      = "SSO Hosting Bucket"
    STAGE     = var.env
    CreatedBy = "Terraform"
  }
}

resource "random_string" "s3_tail" {
  length  = 8
  special = false
  upper   = false
}

# Uncomment to create a static S3 web page

# resource "aws_s3_bucket_policy" "www" {
#   bucket = aws_s3_bucket.www.id

#   policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Id": "APIGatewayAccess",
#   "Statement": [
#     {
#       "Sid": "IPAllow",
#       "Effect": "Allow",
#       "Principal": {
#         "Service": "apigateway.amazonaws.com"
#       },
#       "Action": [
#         "s3:Get*",
#         "s3:List*"
#       ],
#       "Resource": "${aws_s3_bucket.www.arn}/*"
#     },
#     {
#       "Sid": "BBAllow",
#       "Effect": "Allow",
#       "Principal": {
#         "AWS": [
#             "${aws_iam_user.bitbucket.arn}"
#           ]
#       },
#       "Action": [
#         "s3:*"
#       ],
#       "Resource": [
#         "${aws_s3_bucket.www.arn}/*",
#         "${aws_s3_bucket.www.arn}"
#       ]
#     }
#   ]
# }
# POLICY

# }

