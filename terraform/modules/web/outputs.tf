# Output VPC info for consumption by other configs

output "export" {
  value = {
    api_url = aws_api_gateway_deployment.standard.invoke_url
    waf_id  = aws_waf_web_acl.wafacl.id
  }
}

