resource "aws_iam_policy" "api-fe" {
  name        = "${var.env}-${var.site_name}-api-fe-access-policy"
  description = "Policy for use in AWS lambda functions for accessing S3"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "s3Access",
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*"
            ],
            "Resource": [
                "${aws_s3_bucket.www.arn}/*",
                "${aws_s3_bucket.www.arn}"
            ]
        },
        {
            "Sid": "s3Head",
            "Effect": "Allow",
            "Action": "s3:HeadBucket",
            "Resource": "*"
        },
        {
            "Sid": "kmsAccess",
            "Effect": "Allow",
            "Action": [
              "kms:Decrypt",
              "kms:Encrypt",
              "kms:GenerateDataKey",
              "kms:CreateAlias"
            ],
            "Resource": "${var.s3_kms_arn}"
        }
    ]
}
EOF

}

resource "aws_iam_role" "api-fe-role" {
  name               = "${var.env}-${var.site_name}-api-fe-role"
  assume_role_policy = data.aws_iam_policy_document.api-fe-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "api-fe-policy-attachment" {
  role       = aws_iam_role.api-fe-role.name
  policy_arn = aws_iam_policy.api-fe.arn
}

# Uncomment to create a bitbcuekt user for a static s3 web page

# resource "aws_iam_user" "bitbucket" {
#   name = "${var.env}-${var.site_name}-bitbucket-user"
#   path = "/deployment/"
# }

# resource "aws_iam_user_policy" "bitbucket" {
#   name   = "${var.env}-${var.site_name}-bitbucket-user-policy"
#   user   = aws_iam_user.bitbucket.name
#   policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Action": [
#         "s3:PutObject",
#         "s3:GetObject",
#         "s3:GetEncryptionConfiguration",
#         "s3:DeleteObject",
#         "s3:ListBucket",
#         "s3:ListObject",
#         "s3:*"
#       ],
#       "Resource": [
#         "${aws_s3_bucket.www.arn}/*",
#         "${aws_s3_bucket.www.arn}"
#       ]
#     },
#     {
#       "Effect": "Allow",
#       "Action": [
#         "kms:Decrypt",
#         "kms:Encrypt",
#         "kms:DescribeKey",
#         "kms:*"
#       ],
#       "Resource": [
#         "${var.s3_kms_arn}"
#       ]
#     }
#   ]
# }
# POLICY

# }

resource "aws_iam_policy" "auth-lambda" {
  name   = "${var.env}-${var.site_name}-auth-lambda-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Action": "ssm:*",
          "Resource": "arn:aws:ssm:*:*:parameter/_cb-auth-lambda-auto-service",
          "Effect": "Allow"
      },
      {
          "Effect": "Allow",
          "Action": [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents",
              "ec2:CreateNetworkInterface",
              "ec2:DescribeNetworkInterfaces",
              "ec2:DeleteNetworkInterface"
          ],
          "Resource": "*"
      }
  ]
}
EOF

}

resource "aws_iam_role" "auth-lambda-role" {
  name               = "${var.env}-${var.site_name}-auth-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.auth-lambda-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "auth-lambda-policy-attachment" {
  role       = aws_iam_role.auth-lambda-role.name
  policy_arn = aws_iam_policy.auth-lambda.arn
}

