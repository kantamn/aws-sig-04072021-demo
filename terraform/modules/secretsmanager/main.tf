# Create the secret and load password into secret

resource "aws_secretsmanager_secret" "standard" {
  name = "${var.env}-${var.app}"
  kms_key_id = "${aws_kms_key.secret_encryption.id}"
  tags = {
    STAGE = "${var.env}"
    CreatedBy = "Terraform"
  }
}

resource "aws_secretsmanager_secret_version" "standard" {
  secret_id = "${aws_secretsmanager_secret.standard.id}"
  secret_string = "${jsonencode(var.secret_values)}"
  # lifecycle {
  #   ignore_changes = [secret_string]
  # }
}
