output "info" {
  value = {
    secret_arn     = aws_secretsmanager_secret.standard.arn
    secret_id      = aws_secretsmanager_secret.standard.id
    secret_name    = aws_secretsmanager_secret.standard.name
    secret_key_arn = aws_kms_key.secret_encryption.arn
  }
}

