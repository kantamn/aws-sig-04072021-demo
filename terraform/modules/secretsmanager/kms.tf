resource "aws_kms_key" "secret_encryption" {
  description = "${var.env} KMS key for encrypting secrets manager"
  tags = {
    Name      = "${var.env}-${var.app}-secret-encryption-key"
    CreatedBy = "Terraform"
    STAGE     = var.env
  }
}

