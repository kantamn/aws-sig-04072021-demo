# Derived

variable "env" {
}

variable "app" {
}

variable "store_in_ssm" {
  default = false
}

variable "s3_bucket_arn" {
  default = null
}

variable "s3_kms_arn" {
  default = null
}

data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

