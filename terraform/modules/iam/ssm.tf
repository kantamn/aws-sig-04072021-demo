resource "aws_ssm_parameter" "lambda_role" {
  #count = "${var.store_in_ssm}" ? 1 : 0
  name  = "${var.env}-${var.app}-lambda-role"
  type  = "String"
  value = aws_iam_role.lambda.arn
  tags = {
    STAGE     = var.env
    CreatedBy = "Terraform"
  }
}

