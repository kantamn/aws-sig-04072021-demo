resource "aws_iam_policy" "sts_lambda" {
  name        = "${var.env}-lambda-sts_access"
  description = "Policy for use in AWS lambda functions for assuming STS roles"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "sts:DecodeAuthorizationMessage",
                "sts:GetCallerIdentity"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "sts:*",
            "Resource": "*"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "vpc_lambda" {
  name        = "${var.env}-lambda-vpc_exec_access"
  description = "Policy for use in AWS lambda functions that need VPC access"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "ec2:CreateNetworkInterface",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DeleteNetworkInterface"
            ],
            "Resource": "*"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "ses_lambda" {
  name        = "${var.env}-lambda-ses_access"
  description = "Policy for use in AWS lambda functions that need VPC access"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
              "ses:SendEmail",
              "ses:SendTemplatedEmail",
              "ses:GetTemplate",
              "ses:ListTemplates"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF

}

resource "aws_iam_policy" "s3_lambda" {
  name        = "${var.env}-lambda-s3_access"
  description = "Policy for use in AWS lambda functions that access an s3 bucket"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
              "s3:PutObject",
              "s3:PutObject*",
              "s3:GetObject",
              "s3:GetObject*",
              "s3:GetBucket*",
              "s3:*"
            ],
            "Resource": "${var.s3_bucket_arn}"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "kms_lambda" {
  name        = "${var.env}-lambda-s3_kms_access"
  description = "Policy for use in AWS lambda functions that access an s3 bucket"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
        "Sid": "VisualEditor0",
        "Effect": "Allow",
        "Action": [
            "kms:ListKeys",
            "kms:ListAliases"
        ],
        "Resource": "*"
      },
      {
        "Sid": "VisualEditor1",
        "Effect": "Allow",
        "Action": [
            "kms:Decrypt",
            "kms:GetKeyPolicy",
            "kms:Encrypt",
            "kms:GenerateDataKey",
            "kms:DescribeKey"
        ],
        "Resource": [
            "${var.s3_kms_arn}"
        ]
      }
  ]
}
EOF

}

resource "aws_iam_policy" "lambda_access" {
  name        = "${var.env}-lambda-access"
  description = "IAM policy for users who need to access EC2 instance bastion host"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "lambda:InvokeFunction",
                "lambda:GetFunction",
                "lambda:ListAliases",
                "lambda:GetFunctionConfiguration"
            ],
            "Resource": "arn:aws:lambda:*:*:function:*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "lambda:ListFunctions",
            "Resource": "*"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "secretsmanager_lambda" {
  name        = "${var.env}-secrets-manager-access"
  description = "IAM policy for users who need to use to get access to secrets manager"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetSecretValue",
                "secretsmanager:ListSecrets",
                "secretsmanager:DescribeSecret"
            ],
            "Resource": "arn:aws:secretsmanager:*:*:secret:${var.env}-*"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "sns_lambda" {
  name        = "${var.env}-sns-access"
  description = "IAM policy for users who need to use to get access to publish to SNS"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "sns:Publish",
                "sns:GetTopicAttributes"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "sns:ListTopics",
            "Resource": "*"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "stepfunction_lambda" {
  name        = "${var.env}-step-function-access"
  description = "IAM policy to allow initiation of step functions"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "states:StartExecution",
                "states:StopExecution"
            ],
            "Resource": "*"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "ddb_lambda" {
  name        = "${var.env}-ddb-access"
  description = "IAM policy to allow interacting with DynamoDB"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "dynamodb:PutItem",
                "dynamodb:GetShardIterator",
                "dynamodb:UpdateItem",
                "dynamodb:GetRecords",
                "dynamodb:Query",
                "dynamodb:GetItem"
            ],
            "Resource": "*"
        }
    ]
}
EOF

}

