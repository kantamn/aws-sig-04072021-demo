provider "aws" {
  version = "~> 3.22.0"
  region  = "us-west-1"
}

terraform {
  required_version = "<= 0.13.4"
  backend "s3" {
    bucket = "test-aws-sig-demo-terraform"
    key    = "demo.tfstate"
    region = "us-west-1"
  }
}

# This portion should be commented out until a remote state file is established
# data "terraform_remote_state" "infrastructure" {
#    backend = "s3"
#    config = {
#  		bucket  = "test-aws-sig-demo-terraform"
#      key     = "sapep.tfstate"
#      region = "us-west-1"
#   }
# }
